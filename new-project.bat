@rem  new-project.bat
@rem	Bradley Wayne Moore
@rem	http://orangexception.com/

@rem	This script creates directories for a new MVC project.

@rem	Change ProjectName to your new folder name.
@rem		This value can include spaces.
@rem		You do not need to add single or double quotes.

@rem	Frameworks and Libraries are commented below. Uncomment to load.
@rem	Railo hooks are added below. Uncomment to load.

set	ProjectName=ORMTestApp
set	ProjectPath=M:\sites

@rem	Project Directories
mkdir	"%ProjectPath%\%ProjectName%\"
mkdir	"%ProjectPath%\%ProjectName%\build"
mkdir	"%ProjectPath%\%ProjectName%\package"
mkdir	"%ProjectPath%\%ProjectName%\src"
mkdir	"%ProjectPath%\%ProjectName%\src\%ProjectName%"
mkdir	"%ProjectPath%\%ProjectName%\src\%ProjectName%\app\"
mkdir	"%ProjectPath%\%ProjectName%\src\%ProjectName%\app\assets\"
mkdir	"%ProjectPath%\%ProjectName%\src\%ProjectName%\app\assets\documents\"
mkdir	"%ProjectPath%\%ProjectName%\src\%ProjectName%\app\assets\images\"
mkdir	"%ProjectPath%\%ProjectName%\src\%ProjectName%\app\assets\javascripts\"
mkdir	"%ProjectPath%\%ProjectName%\src\%ProjectName%\app\assets\stylesheets\"
mkdir	"%ProjectPath%\%ProjectName%\src\%ProjectName%\app\config\"
mkdir	"%ProjectPath%\%ProjectName%\src\%ProjectName%\app\controllers\"
mkdir	"%ProjectPath%\%ProjectName%\src\%ProjectName%\app\helpers\"
mkdir	"%ProjectPath%\%ProjectName%\src\%ProjectName%\app\layouts\"
mkdir	"%ProjectPath%\%ProjectName%\src\%ProjectName%\app\models\"
mkdir	"%ProjectPath%\%ProjectName%\src\%ProjectName%\app\services\"
mkdir	"%ProjectPath%\%ProjectName%\src\%ProjectName%\app\views\"
mkdir	"%ProjectPath%\%ProjectName%\src\%ProjectName%\assets\"
mkdir	"%ProjectPath%\%ProjectName%\src\%ProjectName%\lib\"

@rem	orangexception libraries
@rem	mkdir	"%ProjectPath%\%ProjectName%\src\%ProjectName%\lib\orangexception"
@rem	xcopy "%ProjectPath%\lib\orangexception\assets\app" "%ProjectPath%\%ProjectName%\src\%ProjectName%\app" /E /I /Y
@rem	xcopy "%ProjectPath%\lib\orangexception\assets\lib\orangexception\assets-builder" "%ProjectPath%\%ProjectName%\src\%ProjectName%\lib\orangexception\assets" /E /I /Y
@rem	xcopy "%ProjectPath%\lib\orangexception\assets\lib\orangexception\yuicompressor" "%ProjectPath%\%ProjectName%\src\%ProjectName%\lib\orangexception\yuicompressor" /E /I /Y
@rem	xcopy "%ProjectPath%\lib\orangexception\datatables" "%ProjectPath%\%ProjectName%\src\%ProjectName%\lib\orangexception\datatables" /E /I /Y
@rem	xcopy "%ProjectPath%\lib\orangexception\sprite\lib\orangexception\sprite" "%ProjectPath%\%ProjectName%\src\%ProjectName%\lib\orangexception\sprite" /E /I /Y

@rem	JavaLoader (used by assets in orangexception libraries)
@rem	xcopy "%ProjectPath%\lib\javaloader\javaloader" "%ProjectPath%\%ProjectName%\src\%ProjectName%\lib\javaloader" /E /I /Y

@rem	ColdFusion Foundation
@rem	xcopy "%ProjectPath%\foundation\ColdFusion" "%ProjectPath%\%ProjectName%\src\ColdFusion" /E /I /Y

@rem	Ant Build Script
@rem	copy "%ProjectPath%\foundation\build.xml" "%ProjectPath%\%ProjectName%\src\%ProjectName%\build.xml"

@rem	ColdSpring
@rem	xcopy "%ProjectPath%\lib\coldspring\coldspring" "%ProjectPath%\%ProjectName%\src\%ProjectName%\lib\coldspring" /E /I /Y
@rem	copy "%ProjectPath%\lib\coldspring\coldspring.xml" "%ProjectPath%\%ProjectName%\src\%ProjectName%\app\config\coldspring.xml"

@rem	ValidateThis
@rem	xcopy "%ProjectPath%\lib\ValidateThis\ValidateThis" "%ProjectPath%\%ProjectName%\src\%ProjectName%\lib\ValidateThis" /E /I /Y



@rem	MVC Frameworks
@rem		Choose a framework and remove @rem blocks before running.

@rem	ColdBox
@rem	mkdir "%ProjectPath%\%ProjectName%\src\%ProjectName%\lib\coldbox\system"
@rem	xcopy "%ProjectPath%\lib\coldbox\coldbox\system" "%ProjectPath%\%ProjectName%\src\%ProjectName%\lib\coldbox\system" /E /I /Y
@rem	copy "%ProjectPath%\lib\coldbox\Application.cfc" "%ProjectPath%\%ProjectName%\src\%ProjectName%\Application.cfc"
@rem	copy "%ProjectPath%\lib\coldbox\index.cfm" "%ProjectPath%\%ProjectName%\src\%ProjectName%\index.cfm"
@rem	xcopy "%ProjectPath%\lib\coldbox\app" "%ProjectPath%\%ProjectName%\src\%ProjectName%\app" /E /I /Y

@rem	FW/1
@rem	xcopy "%ProjectPath%\lib\fw1\org" "%ProjectPath%\%ProjectName%\src\%ProjectName%\lib\org" /E /I /Y
@rem	copy "%ProjectPath%\lib\fw1\Application.cfc" "%ProjectPath%\%ProjectName%\src\%ProjectName%\Application.cfc"
@rem	copy "%ProjectPath%\lib\fw1\index.cfm" "%ProjectPath%\%ProjectName%\src\%ProjectName%\index.cfm"



@rem	Railo ROOT Symbolic Link
@rem	rmdir M:\opt\railo\tomcat\webapps\ROOT /S /Q
@rem	mklink /D M:\opt\railo\tomcat\webapps\ROOT "%ProjectPath%\%ProjectName%\src\%ProjectName%\"

@rem	Restart Railo and Open Application
@rem	net stop "Apache Tomcat Railo"
@rem	net start "Apache Tomcat Railo"
@rem	start http://127.0.0.1:8888/