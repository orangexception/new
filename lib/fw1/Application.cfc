﻿component extends="lib.org.corfield.framework" {

	//	Session Settings
	this.sessionManagement=	true;
	this.sessionTimeout=	createTimeSpan( 0 , 0 , 30 , 0 );
	this.setClientCookies=	true;

	// controllers/layouts/services/views are in this folder (allowing for non-empty context root):
	variables.framework=	{
		base=	getDirectoryFromPath( CGI.SCRIPT_NAME ).replace( getContextRoot(), '' ) & 'app'
	};

}