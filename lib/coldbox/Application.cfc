component	extends=	"lib.coldbox.system.ColdBox"	{

	//	Application Name
	this.name=	hash( getCurrentTemplatePath() );

	//	Session Settings
	this.sessionManagement=	true;
	this.sessionTimeout=	createTimeSpan( 0 , 0 , 30 , 0 );
	this.setClientCookies=	true;

	//	Mappings
	this.mappings[ "/coldbox" ]=	ExpandPath( "/lib/coldbox" );

	//	ColdBox Settings
	COLDBOX_APP_ROOT_PATH=	"/app";
	COLDBOX_APP_MAPPING=	"/app";
	COLDBOX_CONFIG_FILE=	"/app/config/ColdBoxConfiguration";

	include	"/app/helpers/applicationHelper.cfm";

	function onApplicationStart () {
		//	Increase Request Timeout
		requestTimeoutHelper( 91 );

		//	Load ColdBox
		loadColdBox();

		return	true;
	}

	function onRequestStart ( targetPage ) {
		//	Reload ColdBox Check
		reloadChecks();

		//	Page Request Check
		if( REFindNoCase( "\/index.cfm" , targetPage ) ) {
			//	Route to ColdBox
			processColdBoxRequest();

		}

		return	true;
	}

}
