component {

  //	ColdBox Configuration Settings
	function configure () {

		//	ColdBox Settings
		coldbox=	{	//	Application
						appName=		"StandardNewProject",
						defaultEvent=	"main.index" ,

						//	Development
						debugMode=		true ,
						debugPassword=	"" ,
						reinitPassword=	"" ,
						handlersIndexAutoReload=	true ,

						//	Application Emulator Events
						requestStartHandler=		"application_emulator.requestStart" ,
						requestEndHandler=			"" ,
						applicationStartHandler=	"application_emulator.applicationStart" ,
						applicationEndHandler=		"" ,
						sessionStartHandler=		"" ,
						sessionEndHandler=			"" ,

						//	Exceptions
						customErrorTemplate=	"" ,
						exceptionHandler=		"" ,
						onInvalidEvent=			"" ,
						missingTemplateHandler=	"" ,

						//	Caching
						handlerCaching=	false ,
						eventCaching=	false
		};

		//	Layout Settings
		layoutSettings=	{	defaultLayout=	"default.cfm" };

		//	Conventions
		conventions=	{	//	Use Standard MVC Terminology
							handlersLocation=	"controllers" };

		//	Inversion of Control
		ioc=	{	framework=		"coldspring" ,
					reload=			true ,
					objectCaching=	false ,
					definitionFile=	"/app/config/coldspring.xml" };

		//	Interceptors
		interceptors=	[	//	ValidateThis
							{	class=		"ValidateThis.extras.coldbox.ColdBoxValidateThisInterceptor" ,
								properties=	{	definitionPath=	"/app/models/validation/"	} } ];

		//	Debugging Settings
		debugger=	{	expandedRCPanel=	true };

	}

}
