component {

  //	Application Helper
	include	"/app/helpers/applicationHelper.cfm";

	//	Application Start Handler
	function applicationStart ( event , rc ) {
		//	Increase Request Timeout
		requestTimeoutHelper( 121 );

		//	Application Defaults
		param	name=	"application.assetsBuilt"
				default=	"false";

		//	Lock Entire Application
		lock	scope=		"application"
				type=		"exclusive"
				timeout=	"31" {

			//	Build Assets
			if(	application.assetsBuilt == false
			||	StructKeyExists( rc , "reloadAssets" ) ) {
				var	oAssetsBuilder=	new	lib.orangexception.assets.AssetsBuilder();
					oAssetsBuilder.run();

			}

		}

	}

	//	Request Start Handler
	function requestStart ( event , rc ) {
		//	Scrub the Request Clean
		rc=	scrubRequestStructure( rc );

		//	Parse Request Variables into Arrays and Structures
		rc=	createNestedParamStructure( rc );

		//	Reset High Risk XSS Issues

		//	Security Checkpoint ( Security May Redirect Event )
		securityCheck( argumentCollection=	arguments );

		return	true;
	}

	//	Private Functions
	private function securityCheck( event , rc ) {
		//	Define Exclusion List
		var	asExclusionEvents=	[	"main.index" ];

		//	Assumption
		var	bUserSignedIn=	false;

		//	Verify User Signed Into System
		if( false ) {
			//	User is Signed Into System
			bUserSignedIn=	true;

		}

	}


	private struct function scrubRequestStructure (	stTarget ,
													bModifyTarget=	false ) {
		//	Reference Target Structure
		var	stResult=	stTarget;

		//	Modify Target Structure Check
		if( bModifyTarget ) {
			//	Create Duplicate Structure to Leave Target Structure as Found
			stResult=	Duplicate( stTarget );

		}

		//	Loop Through Result Structure
		for( var key in stResult ) {
			//	Scrub Structures
			if( isStruct( stResult[ key ] ) ) {
				//	Scrub Inner Structure by Reference
				scrubRequestStructure( stResult[ key ] , false );

			}
			//	Scrub Simple Values
			else if ( isSimpleValue( stResult[ key ] ) ) {
				//	HTML Edit Format
				stResult[ key ]=	htmlEditFormat( stResult[ key ] );
				//	Remove Tags - This is a dirty script. It may remove unintended items.
				stResult[ key ]=	REReplaceNoCase(	stResult[ key ] ,
														"<[^>]*>" , "" ,
														"all" );

			}

		}

		return	stResult;
	}

	//	I parse structures and arrays from requests and turn them into their proper form.
	//	This code was modified from the source code.
	//	http://stackoverflow.com/questions/5155929/coldfusion-converting-form-values-into-a-struct
	private struct function createNestedParamStructure ( rc ) {
		//	Loop Through Request Collection
		for( var key in rc ) {
			//	Find Structure
			if(	Find( "[" , key )
			&&	Right( key , 1 ) == "]" ) {

				//	Key Name
				var	name=	SpanExcluding( key , "[" );

				//	Split Key Into Array for Multilevel Support
				var	nested=	ListToArray(	ReplaceList(	key ,
															name & "[,]" ,
															"" ) ,
											"[" ,
											true );

				//	Create Structure for Name
				if( StructKeyExists( rc , name ) == false ) {
					rc[ name ]=	{};

				}

				//	Reference to Name in RC
				var	struct=	rc[ name ];

				//	Loop Through Nested Items
				for( var item in nested ) {

					//	Create Inner Structure
					if(	StructKeyExists( struct , item ) == false
					||	isStruct( struct[ item ] ) == false ) {
						struct[ item ]=	{};

					}

					//	Keep Reference to Parent Structure
					var	last=	struct;

					//	Update Reference to Inner Structure
					struct=	struct[ item ];

				}

				//	Update Parent Structure
				last[ item ]=	rc[ key ];

				//	Delete the original key so that it doesn't show up in the params
				StructDelete( rc , key , false );

			}

		}

		return rc;
	}

}
